import random

class Yahtzee:
    def __init__(self):
        self.dice = [0] * 5
        self.roll_count = 0
        self.scorecard = [
            {'ones': None, 'twos': None, 'threes': None, 'fours': None, 'fives': None, 'sixes': None,
             'three_of_kind': None, 'four_of_kind': None, 'full_house': None, 'small_straight': None,
             'large_straight': None, 'yahtzee': None, 'chance': None},
            {'ones': None, 'twos': None, 'threes': None, 'fours': None, 'fives': None, 'sixes': None,
             'three_of_kind': None, 'four_of_kind': None, 'full_house': None, 'small_straight': None,
             'large_straight': None, 'yahtzee': None, 'chance': None}
        ]
        self.current_turn = 0

    def roll_dice(self, dice_to_reroll):
        if self.roll_count >= 3:
            return self.dice
        for i in range(5):
            if dice_to_reroll[i]:
                self.dice[i] = random.randint(1, 6)
        self.roll_count += 1
        return self.dice

    def calculate_possible_scores(self):
        scores = {}
        counts = [self.dice.count(i) for i in range(1, 7)]
        scores['ones'] = counts[0] * 1
        scores['twos'] = counts[1] * 2
        scores['threes'] = counts[2] * 3
        scores['fours'] = counts[3] * 4
        scores['fives'] = counts[4] * 5
        scores['sixes'] = counts[5] * 6
        scores['three_of_kind'] = sum(self.dice) if any(c >= 3 for c in counts) else 0
        scores['four_of_kind'] = sum(self.dice) if any(c >= 4 for c in counts) else 0
        scores['full_house'] = 25 if (3 in counts and 2 in counts) else 0
        scores['small_straight'] = 30 if any(set(seq).issubset(set(self.dice)) for seq in [[1, 2, 3, 4], [2, 3, 4, 5], [3, 4, 5, 6]]) else 0
        scores['large_straight'] = 40 if any(set(seq).issubset(set(self.dice)) for seq in [[1, 2, 3, 4, 5], [2, 3, 4, 5, 6]]) else 0
        scores['yahtzee'] = 50 if any(c == 5 for c in counts) else 0
        scores['chance'] = sum(self.dice)
        return scores

    def select_score(self, player, category, score):
        if player < 0 or player >= len(self.scorecard):
            raise ValueError("Invalid player index")
        self.scorecard[player][category] = score
        self.next_turn()

    def next_turn(self):
        self.roll_count = 0
        self.dice = [0] * 5
        self.current_turn = (self.current_turn + 1) % 2
