# Dice Thrills

Project Name - Dice Thrills
Team         - Virtuso

This is a total learning based project in which i explored Golang, C#, flask, JavaScript, HTML, CSS


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

'''
git clone https://gitlab.com/bhavya_95/dice-thrills.git
cd dice-thrills/
'''

## Structure of repository

logic.py    - Command line version of game in Python.

logic.go    - Command line version of game in Go language.

logic.exe   - Command line version of game in C-Sharp.

static      - style.css

            - Background pictures

templates   - index.html

backend.py  - For GUI version.


## CLV in Go language

Install packages to run GO on Linux

```
sudo apt-get update
sudo apt-get install -y golang-go
wget https://golang.org/dl/go1.20.4.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.20.4.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.profile
source ~/.profile
go version
```

For official Doctumentation:

'''
https://go.dev/doc/install
'''

To play

'''
cd dice-thrills
go run logic.go
'''

## CLV in C -  Sharp

Install environment to run files

'''
sudo apt-get update
sudo apt install mono-complete
mono --version
'''

For official Doctumentation - 

'''
https://dotnet.microsoft.com/en-us/languages/csharp
'''
To play

'''
cd dice-thrills
mono logic.exe
'''

## CLV in Python

To play the game in python on command line
'''
cd dice-thrills
python3 logic.py
'''
## GUI version

Num Thrills is a game where you roll five numbers  and try to get the highest score possible by achieving certain combinations. Here are the rules:

Objective: The goal is to score the highest total points by rolling five nums and achieving specific combinations.

Game Play: Each player takes turns rolling. On each turn, you can roll the nums is  up to three times. After each roll, you can choose to hold any of the dice and re-roll the rest.

Categories: There are thirteen categories to score in, such as Ones, Twos, Threes, Four of a Kind, Full House, etc. Each category has specific scoring rules.

Scoring: The score for each category is based on the result of your dice rolls.

Scoring Examples:

Ones: Sum of all nums showing 1.
Twos: Sum of all nums showing 2.
Threes: Sum of all nums showing 3.
Fours: Sum of all nums showing 4.
Fives: Sum of all nums showing 5.
Sixes: Sum of all nums showing 6.
Three of a Kind: Sum of all nums if at least three numbers show the same number.
Four of a Kind: Sum of all nums if at least four numbers show the same number.
Full House: A combination of three of a kind and a pair. Scores 25 points if achieved.
Yahtzee: All five nums showing the same number. Scores 50 points if achieved.
End of Game: The game ends when all categories are filled. The player with the highest total score wins.

## Contributor

Alavala Bhavya Kesini
WE Cohort-6
