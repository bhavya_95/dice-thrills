package main

import (
	"fmt"
	"math/rand"
	"strings"
	"strconv"
	"time"
)

type DiceThrills struct {
	dice  [5]int
	scorecard [2]map[string]int
	rollCount int
	currentTurn int
	numPlayers int
	playerScores [2]int
}

func NewYahtzee() *DiceThrills {
	game := &DiceThrills{
		scorecard: [2]map[string]int{{}, {}},
		numPlayers: 2,
	}
	rand.Seed(time.Now().UnixNano())
	return game
}

func (y *DiceThrills) nextTurn() {
	y.currentTurn = (y.currentTurn + 1) % y.numPlayers
}

func (y *DiceThrills) rollDice(diceToReroll []bool) {
	if diceToReroll == nil {
		diceToReroll = make([]bool, 5)
		for i := range diceToReroll {
			diceToReroll[i] = true
		}
	}
	for i := 0; i < 5; i++ {
		if diceToReroll[i] {
			y.dice[i] = rand.Intn(6) + 1
		}
	}
	y.rollCount++
}

func (y *DiceThrills) calculateScore() map[string]int {
	scores := map[string]int {
	"ones":      	  y.countDice(1) * 1,
	"twos": 	  y.countDice(2) * 2,
	"threes":         y.countDice(3) * 3,
	"fours":          y.countDice(4) * 4,
	"fives":          y.countDice(5) * 5,
	"sixes":          y.countDice(6) * 6,
	"three_of_kind":  sumDice(y.dice[:]) * y.hasCount(3),
	"four_of_kind":   sumDice(y.dice[:]) * y.hasCount(4),
	"full_house":     25 * y.isFullHouse(),
	"small_straight": 30 * y.isSmallStraight(),
	"large_straight": 40 * y.isLargeStraight(),
	"yahtzee":        50 * y.hasCount(5),
	"chance":         sumDice(y.dice[:]),
	}

	for category := range scores {
		if _, exists := y.scorecard[y.currentTurn][category]; exists {
			delete(scores, category)
		}
	}
	return scores
}

func (y *DiceThrills) countDice(value int) int {
	count := 0
	for _, die := range y.dice {
		if die == value {
			count++
		}
	}
	return count
}
	

func (y *DiceThrills) hasCount(count int) int{
	for _, die := range y.dice {
		if y.countDice(die) >= count {
			return 1
		}
	}
	return 0
}

func (y *DiceThrills) isFullHouse() int {
	counts := make(map[int]int)
	for _, die := range y.dice {
		counts[die]++
	}
	values := []int{}
	for _, count := range counts {
		values = append(values, count)
	}
	if len(values) == 2 && (values[0] == 2 && values[1] == 3 || values[0] == 3 && values[1] == 2) {
		return 1
	}
	return 0
}

func (y *DiceThrills) isSmallStraight() int {
	straightSets := [][]int{
		{1, 2, 3, 4},
		{2, 3, 4, 5},
		{3, 4, 5, 6},
	}
	for _, set := range straightSets {
		match := true
		for _, value := range set {
			if y.countDice(value) == 0 {
				match = false
				break
			}
		}
		if match {
			return 1
		}
	}
	return 0
}

func (y *DiceThrills) isLargeStraight() int {
	largeStraightSets := [][]int{
		{1, 2, 3, 4, 5},
		{2, 3, 4, 5, 6},
	}
	for _, set := range largeStraightSets {
		match := true
		for _, value := range set {
			if y.countDice(value) == 0 {
				match = false
				break
			}
		}
		if match {
			return 1
		}
	}
	return 0
}

func sumDice(dice []int) int {
	total := 0
	for _, die := range dice {
		total += die
	}
	return total
}

func (y *DiceThrills) calculateUpperSectionTotal() int {
	upperSectionKeys := []string{"ones", "twos", "threes", "fours", "fives", "sixes"}
	total := 0
	for _, key := range upperSectionKeys {
		total += y.scorecard[y.currentTurn][key]
	}
	return total
}

func (y *DiceThrills) updateUpperSectionTotal() {
	upperTotal := y.calculateUpperSectionTotal()
	if upperTotal >= 63 {
		y.scorecard[y.currentTurn]["bonus"] = 50
	} else {
		y.scorecard[y.currentTurn]["bonus"] = 0
	}
	y.scorecard[y.currentTurn]["upper_total"] = upperTotal
}

func (y *DiceThrills) calculateLowerSectionTotal() int {
	lowerSectionKeys := []string{"three_of_kind", "four_of_kind", "full_house", "small_straight", "large_straight", "yahtzee", "chance"}
	total := 0
	for _, key := range lowerSectionKeys {
		total += y.scorecard[y.currentTurn][key]
	}
	return total
}

func (y *DiceThrills) calculateTotal() int {
	return y.calculateLowerSectionTotal() + y.calculateUpperSectionTotal() + y.scorecard[y.currentTurn]["bonus"]
}

func (y *DiceThrills) play() {
	for !y.isGameOver() {
		fmt.Printf("Player %d's turn\n", y.currentTurn+1)
		var diceToReroll []bool
		for i := 0; i < 3; i++ {
			y.rollDice(diceToReroll)
			fmt.Printf("Roll %d: %v\n", i+1, y.dice)
			if i < 2 {
				var rerollInput string
				fmt.Print("Enter dice to reroll (e.g., 1 3 5): ")
				fmt.Scanln(&rerollInput)
				if rerollInput != "" {
					rerollIndices := strings.Fields(rerollInput)
					diceToReroll = make([]bool, 5)
					for _, index := range rerollIndices {
						idx, _ := strconv.Atoi(index)
						if idx >= 1 && idx <= 5 {
							diceToReroll[idx-1] = true
						}
					}
				} else {
					break
				}
			}
		}

		scores := y.calculateScore()
		y.displayScores(scores)

		for {
			var category string
			fmt.Print("Enter category to score: ")
			fmt.Scanln(&category)
			if score, exists := scores[category]; exists {
				y.scorecard[y.currentTurn][category] = score
				fmt.Printf("Scored %d points in %s\n", score, category)
				break
			} else {
				fmt.Println("Invalid category. Please try again.")
			}
		}
		y.updateUpperSectionTotal()
		y.nextTurn()
		y.displayAllScores()
	}

	fmt.Println("Final Scores:")
	y.displayAllScores()
	for i := 0; i < y.numPlayers; i++ {
		fmt.Printf("Player %d's total score: %d\n", i+1, y.calculateTotal())
	}
}

func (y *DiceThrills) displayScores(scores map[string]int) {
	for category, score := range scores {
		fmt.Printf("%s: %d\n", category, score)
	}
}

func (y *DiceThrills) displayAllScores() {
	for playerIndex, playerScorecard := range y.scorecard {
		fmt.Printf("Player %d: %v\n", playerIndex+1, playerScorecard)
	}
}

func (y *DiceThrills) isGameOver() bool {
	return len(y.scorecard[1]) == 13
}

func main() {
	game := NewYahtzee()
	game.play()
}

